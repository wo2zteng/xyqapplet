//index.js
//获取应用实例
const app = getApp()

Page({
  options: {
    addGlobalClass: true,
  },
  data: {
    StatusBar: app.globalData.StatusBar,
    CustomBar: app.globalData.CustomBar,
    TabCur: 0,
    scrollLeft: 0,
    clientHeight: 0,
    naviHeight: 0,
    sphereOwnerHeight: 0,
    elements: [{
      title: '学习打卡',
      name: 'study',
      color: 'cyan',
      icon: 'colorlens'
    },
    {
      title: '考研打卡',
      name: 'postgraduate',
      color: 'blue',
      icon: 'edit'
    },
    {
      title: '考证打卡',
      name: 'certificate',
      color: 'purple',
      icon: 'newsfill'
    },
    {
      title: '游戏打卡',
      name: 'game',
      color: 'mauve',
      icon: 'game'
    },
    {
      title: '夜聊打卡',
      name: 'night chat',
      color: 'pink',
      icon: 'community'
    },
    {
      title: '其他打卡',
      name: 'others',
      color: 'brown',
      icon: 'mark'
    }
    ],
    sphereOwners: [
      {
        name: '陈思奇',
        getIconThumb: 'https://ossweb-img.qq.com/images/lol/web201310/skin/big10001.jpg',
        getSphereTag: '德语活跃用户',
        diary_count: 365,
        fans_count: 704
      },
      {
        name: '杨路',
        getIconThumb: 'https://ossweb-img.qq.com/images/lol/img/champion/Morgana.png',
        getSphereTag: '德语活跃用户',
        diary_count: 365,
        fans_count: 704
      },
      {
        name: '陈思奇',
        getIconThumb: 'https://ossweb-img.qq.com/images/lol/web201310/skin/big10001.jpg',
        getSphereTag: '德语活跃用户',
        diary_count: 365,
        fans_count: 704
      },
      {
        name: '杨路',
        getIconThumb: 'https://ossweb-img.qq.com/images/lol/img/champion/Morgana.png',
        getSphereTag: '德语活跃用户',
        diary_count: 365,
        fans_count: 704
      },
      {
        name: '陈思奇',
        getIconThumb: 'https://ossweb-img.qq.com/images/lol/web201310/skin/big10001.jpg',
        getSphereTag: '德语活跃用户',
        diary_count: 365,
        fans_count: 704
      },
      {
        name: '杨路',
        getIconThumb: 'https://ossweb-img.qq.com/images/lol/img/champion/Morgana.png',
        getSphereTag: '德语活跃用户',
        diary_count: 365,
        fans_count: 704
      },
      {
        name: '陈思奇',
        getIconThumb: 'https://ossweb-img.qq.com/images/lol/web201310/skin/big10001.jpg',
        getSphereTag: '德语活跃用户',
        diary_count: 365,
        fans_count: 704
      },
      {
        name: '杨路',
        getIconThumb: 'https://ossweb-img.qq.com/images/lol/img/champion/Morgana.png',
        getSphereTag: '德语活跃用户',
        diary_count: 365,
        fans_count: 704
      },
      {
        name: '陈思奇',
        getIconThumb: 'https://ossweb-img.qq.com/images/lol/web201310/skin/big10001.jpg',
        getSphereTag: '德语活跃用户',
        diary_count: 365,
        fans_count: 704
      },
      {
        name: '杨路',
        getIconThumb: 'https://ossweb-img.qq.com/images/lol/img/champion/Morgana.png',
        getSphereTag: '德语活跃用户',
        diary_count: 365,
        fans_count: 704
      },
      {
        name: '陈思奇',
        getIconThumb: 'https://ossweb-img.qq.com/images/lol/web201310/skin/big10001.jpg',
        getSphereTag: '德语活跃用户',
        diary_count: 365,
        fans_count: 704
      },
      {
        name: '杨路',
        getIconThumb: 'https://ossweb-img.qq.com/images/lol/img/champion/Morgana.png',
        getSphereTag: '德语活跃用户',
        diary_count: 365,
        fans_count: 704
      },
    ]
  },
  //事件处理函数
  bindViewTap: function() {
    wx.navigateTo({
      url: '../logs/logs'
    })
  },
  onLoad: function () {
    this.setContainerHeight();
  },
  setContainerHeight: function () {
    var that = this;
    wx.getSystemInfo({
      success: function (res) {
        let tabClientHeight = res.windowHeight - 51.4 - 50 - 45 - 103 - 50;
        that.setData({
          clientHeight: tabClientHeight,
          naviHeight: tabClientHeight - 90,
          sphereOwnerHeight: tabClientHeight - 40
        });
      }
    });
  },
  tabSelect(e) {
    this.setData({
      TabCur: e.currentTarget.dataset.id,
      scrollLeft: (e.currentTarget.dataset.id - 1) * 60
    })
  }
})
